<?php

class Crud extends CI_Controller{

    public function index(){
        $data['testimonial_details']=$this->crud_model->getAllTestimonials();
        $this->load->view('crud_view',$data);
    }
    public function addTestimonial(){
        $this->form_validation->set_rules('title','Title Name','required');
        $this->form_validation->set_rules('description','Description','required');
        $this->form_validation->set_rules('img_link','Image Link','required');


        if($this->form_validation->run() ==false){
            $data_error=[
                'error'=> validation_errors()
            ];
            $this->session->set_flashdata($data_error);
        }
        redirect('crud');
    }
}



?>